from django.views.generic.list import MultipleObjectMixin
from listsearch.forms import ListSearchForm


class ListSearchMixin(MultipleObjectMixin):

    listsearch_form = "form"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context[self.listsearch_form] = ListSearchForm(self.request.GET)
        return context

    @property
    def query(self):
        return self.request.GET.get("query", None)
