from django.forms import CharField, Form


class ListSearchForm(Form):
    query = CharField(label="Search", max_length=128, required=False)
