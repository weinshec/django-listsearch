#!/usr/bin/env python

from pathlib import Path
from setuptools import setup

root = Path(__file__).parent or "."

with (root / "README.rst").open(encoding="utf-8") as f:
    long_description = f.read()

info = {
    "name":                "django-listsearch",
    "version":             "0.1.0",
    "author":              "Christoph Weinsheimer",
    "author_email":        "weinshec@holodeck2.de",
    "url":                 "https://gitlab.com/weinshec/django-listsearch",
    "packages":            ["listsearch"],
    "provides":            ["listsearch"],
    "description":         "Provide searchbar in django ListViews",
    "long_description":    long_description,
    "keywords":            ["python", "django"],
    "install_requires":    ["django>=2.0"],
}

if __name__ == "__main__":
    setup(**info)
