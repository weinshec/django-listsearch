from django.shortcuts import reverse
from django.test import TestCase
from sampleapp.models import SampleModel


class ListSearchMixinTest(TestCase):

    def setUp(self):
        self.model1 = SampleModel.objects.create(
            first_name="Albert", last_name="Einstein")
        self.model2 = SampleModel.objects.create(
            first_name="Stephen", last_name="Hawking")

    def test_view_has_search_bar(self):
        response = self.client.get(reverse("list"))
        self.assertContains(response, "id_query")

    def test_searchbar_still_contains_last_search(self):
        response = self.client.get(
            reverse("list"), {"query": "some query"})
        self.assertContains(response, "some query")

    def test_can_filter_list_using_main_views_get_queryset(self):
        response = self.client.get(
            reverse("list"), {"query": "Stephen"})
        self.assertNotContains(response, self.model1.last_name)
        self.assertContains(response, self.model2.last_name)
