from django.test import TestCase
from listsearch.forms import ListSearchForm


class ListSearchFormTest(TestCase):

    def test_form_has_query_field(self):
        form = ListSearchForm()
        self.assertIn("query", form.fields)

    def test_form_validates_if_empty(self):
        form = ListSearchForm(data={})
        self.assertTrue(form.is_valid())
