from django.views.generic import ListView
from sampleapp.models import SampleModel
from listsearch.views import ListSearchMixin


class List(ListSearchMixin, ListView):
    model = SampleModel

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)

        if self.query:
            return queryset.filter(first_name__icontains=self.query)

        return queryset
