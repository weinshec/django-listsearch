from django.conf.urls import url
from sampleapp.views import List

urlpatterns = [
    url(r"^list$", List.as_view(), name="list"),
]
